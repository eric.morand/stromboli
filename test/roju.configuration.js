const {default: foo} = require("./foo.plugin.js");

/**
 * @type {Configuration}
 */
const configuration = {
    entryPoint: 'index',
    outputDirectory: 'www',
    routes: [
        [(componentEntryPoint) => {
            return {
                path: `${componentEntryPoint}.html.twig`,
                name: 'content'
            };
        }, foo, `index.html`]
    ]
};

module.exports = configuration;