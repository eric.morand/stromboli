import {Command, createArgument, createOption} from "commander";
import {start} from "./start";

export const main = (argv: readonly string[]) => {
    const program = new Command('stromboli')
        .addArgument(createArgument('componentPath').argOptional().default('.'))
        .addOption(createOption('-c, --configuration-path <configurationPath>'))
        .addOption(createOption('-b, --build-only').default(false))
        .action((componentPath: string, options: {
            configurationPath?: string;
            buildOnly: boolean
        }) => {
            console.log(options);

            return start(componentPath, options);
        });

    program.parse(argv);
}
