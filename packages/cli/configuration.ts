import type {Component, Plugin, Artifact} from "@stromboli/core";

export type Configuration = {
    outputDirectory: string;
    routes: Array<{
        component: Component;
        plugin: Plugin;
        output: string | ((artifact: Artifact) => string);
    }>;
};