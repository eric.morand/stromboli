import {create, get, has} from "browser-sync";
import {emptyDirSync} from "fs-extra";
import type {Plugin, Component} from "@stromboli/core";
import {writeFile} from "fs/promises";
import {createANDMiddleware, createORMiddleware} from "@arabesque/logic-middlewares";
import {createApplication} from "@stromboli/core";
import findupSync from "findup-sync";
import {resolve} from "path";
import {join} from "path/posix";
import type {Configuration} from "./configuration";

const configurationCandidates = [
    'stromboli.configuration.mjs',
    'stromboli.configuration.cjs',
    'stromboli.configuration.js'
];

const resolveConfiguration = (configurationPath: string | null = null): Promise<Configuration | null> => {
    if (configurationPath === null) {
        configurationPath = findupSync(configurationCandidates);
    }

    if (configurationPath === null) {
        return Promise.resolve(null);
    }

    const resolvedConfigurationPath = resolve(configurationPath);

    console.log('RESOLVED CONF PATH', resolvedConfigurationPath);

    return import(resolvedConfigurationPath)
        .then((module: { default: Configuration }) => module.default);
};

export const start = (componentPath: string, options: {
    configurationPath?: string;
    buildOnly: boolean;
}) => {
    const {configurationPath, buildOnly} = options;

    return resolveConfiguration(configurationPath)
        .then((configuration) => {
            console.log('RESOLVED CONF', configuration);

            if (configuration === null) {
                throw new Error('Invalid configuration');
            }

            const outputDirectory = configuration.outputDirectory;

            emptyDirSync(outputDirectory);

            const promises = configuration.routes.map(({component, plugin, output}) => {
                const writeOutput: Plugin = (context, next) => {
                    return Promise.all(context.response.artifacts.map((artifact) => {
                        const outputPath = typeof output === "string" ? `${outputDirectory}/${output}` : `${outputDirectory}/${output(artifact)}`;

                        return writeFile(outputPath, artifact.data).then(() => outputPath);
                    })).then((outputPaths) => {
                        if (has(componentPath)) {
                            get(componentPath).reload(outputPaths);
                        }

                        return next(context);
                    });
                };

                const handleError: Plugin = (context, next) => {
                    console.error(context.response.error);

                    context.response.artifacts = [{
                        name: component.name,
                        data: Buffer.from('')
                    }];

                    return next(context);
                };

                const middleware = createANDMiddleware(
                    createORMiddleware(
                        plugin,
                        handleError
                    ), writeOutput);

                const resolvedComponent: Component = {
                    name: component.name,
                    path: join(componentPath, component.path)
                };

                return middleware({
                    component: resolvedComponent,
                    response: {
                        artifacts: [],
                        dependencies: []
                    }
                }, (context) => {
                    return Promise.resolve(context);
                }).then((context) => {
                    if (!buildOnly) {
                        const application = createApplication(middleware);

                        return application({
                            component: resolvedComponent,
                            filenames: context.response.dependencies
                        });
                    }
                })
            });

            Promise.all(promises).then(() => {
                console.log('INITIAL PASS DONE');
            }).then(() => {
                return new Promise((resolve, reject) => {
                    if (!buildOnly) {
                        return create(componentPath).init({
                            server: outputDirectory,
                            open: false,
                            notify: false
                        }, (error) => {
                            if (error) {
                                reject(error);
                            } else {
                                resolve(undefined);
                            }
                        });
                    }
                });
            });
        });
};