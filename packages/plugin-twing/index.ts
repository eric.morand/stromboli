import type {Plugin} from "@stromboli/core";
import {TwingEnvironment, TwingEnvironmentOptions, TwingLoaderInterface, TwingSource} from "twing";

export const createTwingPlugin = (
    loader: TwingLoaderInterface,
    options: Omit<TwingEnvironmentOptions, "source_map">
): Plugin => {
    return (context, next) => {
        const {response, component} = context;
        const {name, path} = component;

        const optionsWithSourceMap: TwingEnvironmentOptions = {
            ...options,
            source_map: true
        };

        const environment = new TwingEnvironment(loader, optionsWithSourceMap);

        const dependencies: Array<Promise<string>> = [];

        environment.on("template", (templatePath: string, from: TwingSource) => {
            dependencies.push(loader.resolve(templatePath, from));
        });

        return environment.loadTemplate(path)
            .then((template) => {
                return template.render(component);
            })
            .then((output) => {
                const rawSourceMap = JSON.parse(environment.getSourceMap());

                console.log(rawSourceMap);

                return Promise.all(dependencies)
                    .then((dependencies) => {
                        response.artifacts.push({
                            name,
                            data: Buffer.from(output)
                        });

                        response.dependencies.push(...dependencies);

                        return next(context);
                    });
            });

    };
};