export type Component = {
    name: string;
    path: string;
};
