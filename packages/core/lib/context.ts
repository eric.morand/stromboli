import type {RawSourceMap} from "source-map-js";
import type {Component} from "./component";

export type Artifact = {
    name: string;
    data: Buffer;
    sourceMap?: RawSourceMap;
};

export type Context = {
    component: Component;
    response: {
        artifacts: Array<Artifact>;
        dependencies: Array<string>;
        error?: Error
    };
};