import type {Middleware} from "@arabesque/core";
import type {Context} from "./context";

export type Plugin = Middleware<Context>;
