import {createApplication as createArabesqueApplication, Listener, Middleware} from "@arabesque/core";
import type {Context} from "./context";
import {FSWatcher, watch} from "chokidar";
import type {Component} from "./component";

export const createApplication = (middleware: Middleware<Context>) => {
    const listener: Listener<{
        component: Component;
        filenames: Array<string>
    }, Context> = ({component, filenames}, handler) => {
        let watcher: FSWatcher;

        const watchFiles = (filenames: Array<string>) => {
            console.log('WATCHING >', filenames);

            watcher = watch(filenames);

            watcher.on("change", (filename) => {
                console.log(`CHANGE > ${filename}`);

                return watcher.close()
                    .then(() => {
                        console.log('CLOSE');

                        return handler({
                            component,
                            response: {
                                dependencies: [],
                                artifacts: []
                            }
                        });
                    })
                    .then((context) => {
                        watcher = watchFiles(context.response.dependencies);
                    });
            });

            return watcher;
        }

        watcher = watchFiles(filenames);

        return Promise.resolve(watcher.close);
    };

    return createArabesqueApplication(listener, middleware);
};