import type {Plugin} from "@stromboli/core";
import SASS, {Exception} from 'sass';

const {compile} = SASS;

export const createSassPlugin = (): Plugin => {
    return (context, next) => {
        const {component} = context;
        const {name, path} = component;

        try {
            const result = compile(path, {
                sourceMap: true,
                sourceMapIncludeSources: true
            });

            context.response.dependencies.push(...result.loadedUrls.map((loadedUrl) => loadedUrl.pathname));

            context.response.artifacts.push({
                name,
                data: Buffer.from(result.css)
            });
        } catch (error: any) {
            const exception = error as Exception;

            if (exception.span.url) {
                context.response.dependencies.push(exception.span.url.pathname);
            }

            context.response.artifacts = [{
                name,
                data: Buffer.from('')
            }];

            context.response.error = error;
        }

        return next(context);
    }
};