import type {Plugin} from "@stromboli/core";
import {OutputOptions, rollup, RollupOptions} from "rollup";

export const createRollupPlugin = (
    options: Omit<RollupOptions, "input">,
    outputOptions: Omit<OutputOptions, "sourcemap" | "sourcemapExcludeSources">
): Plugin => {
    return (context, next) => {
        console.debug('ROLLUP PLUGIN', context);

        const {response, component} = context;
        const {name, path} = component;

        const actualOptions: RollupOptions = {
            ...options,
            input: path
        };

        const actualOutputOptions: OutputOptions = {
            ...outputOptions,
            sourcemap: true,
            sourcemapExcludeSources: false
        };

        return rollup(actualOptions)
            .then((build) => {
                return build.generate(actualOutputOptions);
            })
            .then((output) => {
                output.output.map((chunkOrAsset) => {
                    if (chunkOrAsset.type === "asset") {
                        // what are we supposed to do with assets?
                    } else {
                        const {code } = chunkOrAsset;
                        const map = chunkOrAsset.map!;

                        response.artifacts.push({
                            name,
                            data: Buffer.from(code)
                        });

                        response.dependencies.push(...map.sources);
                    }
                });

                return next(context);
            });
    };
};