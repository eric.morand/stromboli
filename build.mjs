import {createCommand, createArgument, createOption} from "commander";
import {remove} from "fs-extra";
import {join} from "path";
import {existsSync} from "fs";
import {rollup} from "rollup";
import createVirtualPlugin from "@rollup/plugin-virtual";
import createTypescriptPlugin from "@rollup/plugin-typescript";
import createGeneratePackageJsonPlugin from "rollup-plugin-generate-package-json";

const buildPackage = (
    packageName,
    destination,
    version,
    emitSourceMap
) => {
    console.info(`Building package ${packageName}:${version} into ${destination}...`);

    return remove(destination)
        .then(() => {
            const packagePath = join('packages', packageName);
            const tsConfigPath = join(packagePath, 'tsconfig.json');
            const tsConfigPathExists = existsSync(tsConfigPath);

            const additionalDependencies = packageName === 'core' ? {} : {
                '@stromboli/core': `^${version}`
            };

            let input;

            const plugins = [
                createTypescriptPlugin({
                    target: "es2015",
                    tsconfig: join(packagePath, `tsconfig.json`)
                }),
                createGeneratePackageJsonPlugin({
                    inputFolder: join(packagePath),
                    baseContents: (pkg) => {
                        pkg.version = version;

                        if (packageName === 'cli') {
                            return Object.assign({}, pkg, {
                                files: [
                                    "index.cjs"
                                ]
                            });
                        }

                        return Object.assign({}, pkg, {
                            main: "./index.cjs",
                            types: "./types/index.d.ts",
                            exports: {
                                import: "./index.mjs",
                                default: "./index.cjs",
                                types: "./types/index.d.ts"
                            },
                            files: [
                                "index.cjs",
                                "index.mjs",
                                "types"
                            ]
                        });
                    },
                    additionalDependencies
                })
            ];

            if (packageName === 'cli') {
                input = 'entry';
                plugins.unshift(
                    createVirtualPlugin({
                        [input]: `import {main} from "./${packagePath}/main.ts"; main(process.argv);`
                    })
                );
            } else {
                input = join(packagePath, 'index.ts');
            }

            return rollup({
                input,
                plugins,
                onwarn: () => {
                },
                external: [
                    '@stromboli/core'
                ]
            }).then((bundle) => {
                if (packageName === 'cli') {
                    return bundle.write({
                        file: `${destination}/index.cjs`,
                        format: "commonjs",
                        banner: `#!/usr/bin/env node`
                    })
                }

                return Promise.all([
                    bundle.write({
                        file: `${destination}/index.mjs`,
                        format: "esm"
                    }),
                    bundle.write({
                        file: `${destination}/index.cjs`,
                        format: "commonjs"
                    })
                ]);
            });
        }).then(() => {
            console.info(`Package ${packageName}:${version} successfully built into ${destination}`);

            return join(destination, 'index.mjs');
        });
};

const program = createCommand('node build.mjs')
    .addArgument(createArgument('destination'))
    .addOption(createOption('--version <version>').default('0.0.0'))
    .addOption(createOption('--source-map').default(false))
    .action((destination, options) => {
        const packages = [
            'cli',
            'core',
            'plugin-rollup',
            'plugin-sass',
            'plugin-twing'
        ];

        return Promise.all(
            packages.map((packageName) => {
                return buildPackage(packageName, `${destination}/${packageName}`, options.version)
                    .then(() => {
                        const manifest = {};
                    });
            })
        );
    });

program.parse(process.argv);